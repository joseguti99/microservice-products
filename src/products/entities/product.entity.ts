interface UpdateProductDto {
    name?: string;
    description?: string;
    price?: number;
    sku?: string;
}

export class Product {
    constructor(
        public id: string,
        public name: string,
        public description: string,
        public price: number,
        public sku: string,
    ) {}

    updateWith({ 
        name, 
        description, 
        price, 
        sku
    }: UpdateProductDto){
        this.name = name ?? this.name;
        this.description = description ?? this.description;
        this.price = price ?? this.price;
        this.sku = sku ?? this.sku;
    }

}
