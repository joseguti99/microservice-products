import { Test, TestingModule } from '@nestjs/testing';
import { ProductsController } from './products.controller';
import { ProductsService } from './products.service';
import { CreateProductDto } from './dto/create-product.dto';
import { Product } from './entities/product.entity';
import { UpdateProductDto } from './dto/update-product.dto';

describe('ProductsController', () => {
  let controller: ProductsController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [ProductsController],
      providers: [ProductsService],
    }).compile();

    controller = module.get<ProductsController>(ProductsController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  it('should call the controller GET - all products', async () => {
    const mockGetAllProductsDto: Product[] = [
      new Product('product-uuid-1', 'productName1', 'productDescription1', 100, 'sku-code-11111'),
      new Product('product-uuid-2', 'productName2', 'productDescription2', 200, 'sku-code-22222'),
    ]

    const productsService = { findAll: jest.fn().mockResolvedValue(mockGetAllProductsDto) };
    const productsController = new ProductsController(productsService as any);

    const result = await productsController.findAll();

    expect(result).toEqual(mockGetAllProductsDto);
    expect(productsService.findAll).toHaveBeenCalled();
  })

  it('should call the controller GET - product by id', async () => {
    const mockGetProductByIdDto: Product = new Product('product-uuid-1', 'productName1', 'productDescription1', 100, 'sku-code-11111')

    const productsService = { findOne: jest.fn().mockResolvedValue(mockGetProductByIdDto) };
    const productsController = new ProductsController(productsService as any);

    const result = await productsController.findOne('product-uuid-1')
    expect(result).toEqual(mockGetProductByIdDto);
    expect(productsService.findOne).toHaveBeenCalled();
  })

  it('should call the controller POST - create product', async () => {
    const mockCreateProductDto: CreateProductDto = {
      name: 'new productName',
      description: 'new productDescription',
      sku: 'new-sku-code-12342',
      price: 100
    };

    const productsService = { create: jest.fn().mockResolvedValue(mockCreateProductDto) };

    const productsController = new ProductsController(productsService as any);

    await productsController.create('id-new-product-13412', mockCreateProductDto);

    expect(productsService.create).toHaveBeenCalledWith(mockCreateProductDto);
  });

  it('should call the controller PATCH - update product by id', async () => {
    const mockUpdateProductByIdDto: Product = new Product('product-uuid-1', 'productName1', 'productDescription1', 100, 'sku-code-11111')

    const productsService = { update: jest.fn().mockResolvedValue(mockUpdateProductByIdDto) };
    const productsController = new ProductsController(productsService as any);

    const mockRequestQueryId: string = 'product-uuid-1'

    const mockRequestBody: UpdateProductDto = {
      name: 'product-uuid-1',
      description: 'productDescription1',
      price: 100,
      sku: 'sku-code-11111'
    }

    const result = await productsController.update(mockRequestQueryId, mockRequestBody)

    expect(result).toEqual(mockUpdateProductByIdDto);
    expect(productsService.update).toHaveBeenCalled();
  })

  it('should call the controller DELETE - product by id', async () => {
    const mockDeleteProductByIdDto: Product = new Product('product-uuid-1', 'productName1', 'productDescription1', 100, 'sku-code-11111')

    const productsService = { remove: jest.fn().mockResolvedValue(mockDeleteProductByIdDto) };

    const productsController = new ProductsController(productsService as any);

    const mockRequestQueryId: string = 'product-uuid-1'

    const result = await productsController.remove(mockRequestQueryId);


    expect(productsService.remove).toHaveBeenCalledWith(mockRequestQueryId);
    expect(result).toEqual(mockDeleteProductByIdDto);
  });
});
