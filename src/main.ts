import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { ValidationPipe } from '@nestjs/common';
import  { envs } from './config/envs'

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  
  const validationPipe = new ValidationPipe({
    whitelist: true,
    forbidNonWhitelisted: true,
  })

  app.setGlobalPrefix(envs.path)
  app.useGlobalPipes(validationPipe)
  
  await app.listen(envs.port);

  console.log(`Server running on port ${envs.port}`)
}
bootstrap();
 